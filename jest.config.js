module.exports = {
    collectCoverageFrom: [
        '**/*.test.ts',
    ],
    preset: 'ts-jest',
    setupFilesAfterEnv: [
        '@testing-library/jest-dom/extend-expect',
    ],
    testMatch: [
        '**/*.test.ts',
    ],
    testEnvironment: 'jest-environment-jsdom-fourteen',
    transform: {
        '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest',
    },
    testPathIgnorePatterns: [
        '/node_modules/',
        '/.idea/',
        '/build/',
    ],
    transformIgnorePatterns: [
        '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
        '^.+\\.module\\.(css|sass|scss)$',
    ],
    moduleNameMapper: {
        '^models/(.*)': '<rootDir>/models/$1',
        "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy"
    },
    restoreMocks: true,
    reporters: [
        'default',
        [
            'jest-junit',
            {
                suiteNameTemplate: '{filepath}',
                titleTemplate: '{title}',
                outputDirectory: 'build/test-results',
                outputName: 'junit-unit.xml'
            }
        ]
    ],
    maxWorkers: 4,
    maxConcurrency: 4,
    silent: true,
    forceExit: true
}
