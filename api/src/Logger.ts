import {createLogger, format, transports} from 'winston'

const {Console} = transports

const level = process.env.NODE_ENV === 'development' ? 'debug' : 'warn'

const logger = createLogger({level})

const errorStackFormat = format(info => {
  if (info.stack) {
    console.log(info.stack)
    return false
  }
  return info
})

const consoleTransport = new Console({
  format: format.combine(
    format.colorize(),
    format.simple(),
    errorStackFormat(),
  ),
  level,
})

logger.add(consoleTransport)

export default logger