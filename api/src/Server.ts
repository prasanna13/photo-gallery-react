import express, {Express, Request, Response} from 'express'
import 'express-async-errors'
import path from 'path'
import {databaseConnect} from './server/database'
import errorHandler from './server/prepare/errorHandler'
import middleware from './server/prepare/middleware'
import Repository from './repositories/Repository'
import {Photo} from 'models/Photo'
import LocalFileService from './services/LocalFileService'
import S3FileService from './services/S3FileService'
import PhotoService from './services/PhotoService'
import {photoRoutes} from './routes/photoRoutes'

const createServer = async (): Promise<Express> => {
  const server = express()

  middleware(server)

  const {db} = await databaseConnect()

  const photoRepository = new Repository<Photo>(db, 'photos')
  const uploadFileService = process.env.NODE_ENV === 'development' ? new LocalFileService() : new S3FileService()
  const photoService = new PhotoService(uploadFileService, photoRepository)

  server.use('/api/photos', photoRoutes(photoService))

  const staticDir = path.join(__dirname, '..', '..', 'ui/public')

  server.use(express.static(staticDir))
  server.get('/*', (reqReq: Request, res: Response) => {
    res.sendFile(staticDir + '/index.html')
  })

  server.use(errorHandler)

  return server
}

export {createServer}
