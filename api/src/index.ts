import {createServer} from './Server'

const port = 4000

createServer().then(app => {
  app.listen(port, () => {
    console.info(`Express server started on port: ${port}`)
  })
}).catch(console.error)

