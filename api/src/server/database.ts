import {Db, MongoClient} from 'mongodb'

interface DatabaseConfig {
  databaseUrl: string
  databaseName?: string
  connection?: {
    authSource?: string
    auth?: {
      user: string
      password: string
    }
  }
}

const mongoConfig: DatabaseConfig = {
  databaseUrl: process.env.MONGO_URL || 'mongodb://localhost:27027/photo-gallery',
}

type DBConnect = {
  db: Db
  client: MongoClient
}

export const databaseConnect = async (config: DatabaseConfig = mongoConfig): Promise<DBConnect> => {
  console.log('Connecting to MongoDB: ' + config.databaseUrl)
  const client = new MongoClient(config.databaseUrl, {
    ...config.connection,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  const db = (await client.connect()).db(config.databaseName)
  return {client, db}
}
