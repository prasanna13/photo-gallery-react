import express, {Express} from 'express'
import fileUpload from 'express-fileupload'
import morgan from 'morgan'

const middleware = (server: Express) => {
  server.use(express.json({limit: '1mb'}))
  server.use(express.urlencoded({extended: true}))

  server.use(fileUpload())

  server.use(morgan(process.env.NODE_ENV === 'development' ? 'dev' : 'common'))
}

export default middleware