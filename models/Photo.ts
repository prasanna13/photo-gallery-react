import {Entity} from './Entity'

export interface Photo extends Entity {
  title?: string
  url: Url
}

export type Url = {
  original: string,
  fullHd: string,
  thumbnail: string
}