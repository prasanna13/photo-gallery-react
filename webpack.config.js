const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const autoprefixer = require('autoprefixer')
const path = require('path')

const compilePath = [
    path.resolve(__dirname, 'ui'),
    path.resolve(__dirname, 'models')
]

module.exports = {
    devtool: 'source-map',
    context: path.resolve(__dirname, 'ui'),
    output: {
        publicPath: '/',
        path: path.resolve(__dirname, 'build/ui')
    },
    devServer: {
        historyApiFallback: true,
        proxy: {
            "/api": {
                target: "http://localhost:4000"
            },
        }
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        alias: {
            "icons": path.resolve(__dirname, 'ui/src/assets/icons'),
            "models": path.resolve(__dirname, 'models')
        }
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(ts|tsx)?$/,
                loader: 'tslint-loader',
                include: compilePath,
                options: {
                    fix: true
                }
            },
            {
                test: /\.(t|j)sx?$/,
                include: compilePath,
                use: {
                    loader: 'awesome-typescript-loader'
                }
            },
            {
                enforce: 'pre',
                include: compilePath,
                test: /\.js$/,
                loader: 'source-map-loader'
            },
            {
                test: /\.html$/,
                include: compilePath,
                use: {
                    loader: 'html-loader'
                }
            },
            {
                test: /\.css$/,
                include: compilePath,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    }
                ]
            },
            {
                test: /\.scss$/,
                include: compilePath,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins() {
                                return [autoprefixer()]
                            }
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.svg$/,
                include: path.resolve(__dirname, 'ui/src/assets/icons'),
                use: [
                    {
                        loader: '@svgr/webpack',
                        options: {
                            template: (
                                {template},
                                opts,
                                {imports, componentName, props, jsx, exports}
                            ) => template.ast`
                ${imports}
                
                const ${componentName} = (${props}) => {
                  const ref = React.createRef();
                  React.useLayoutEffect(() => {
                    if (ref.current !== null && !ref.current.getAttribute('viewBox')) {
                      const width = ref.current.getAttribute('width')
                      const height = ref.current.getAttribute('height')
                      ref.current.setAttribute('viewBox', [0, 0, width, height].join(' '))
                    }
                  })
                  
                  props = { ...props, ref, className: props.className };
                  return ${jsx};
                };

                ${exports};
              `
                        }
                    },
                    {
                        loader: 'svg-inline-loader',
                        options: {
                            removeSVGTagAttrs: false
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './public/index.html',
            favicon: './public/favicon.ico',
        }),
        new CopyPlugin({
            patterns: [
                {from: 'public'}
            ]
        }),
        new MiniCssExtractPlugin({
            filename: 'style.css'
        }),
        new CompressionPlugin()
    ]
}
