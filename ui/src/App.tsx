import React, {ReactElement} from 'react'
import './assets/scss/main.scss'
import PhotosPage from './components/PhotosPage'
import Navbar from './components/Navbar'

const App: React.FC = (): ReactElement => {
  return (
    <>
      <Navbar />
      <PhotosPage />
    </>
  )
}

export default App
