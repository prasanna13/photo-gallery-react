import React, {ReactElement, useEffect, useState} from 'react'
import {Photo} from 'models/Photo'
import {getPhotos} from '../repositories/photos'

const PhotosPage: React.FC = (): ReactElement => {
  const [photos, setPhotos] = useState<Photo[]>([])

  useEffect(() => {
    getPhotos().then(setPhotos)
  }, [])

  return photos && (
    <div className="container">
      <div className="row">
        {photos.map(p => {
          return (
            <div className="col-3 mt-4">
              <div className="card card-hover-effect justify-content-center position-relative" style={{height: '220px', width: '340px'}}>
                <div className="row justify-content-center align-items-center">
                  <img src={p.url.thumbnail} alt={p.url.original} style={{width: '320px', height: '200px', display: 'block'}}/>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default PhotosPage
