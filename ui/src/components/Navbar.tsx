import React, {ReactElement} from 'react'
import {ReactComponent as AddPhoto} from 'icons/plus.svg'

const Navbar: React.FC = (): ReactElement => {
  return (
    <nav className="navbar sticky-top navbar-light bg-light">
      <div className="container-fluid justify-content-center">
        <AddPhoto/>
        {/*<button className="btn" on:click={() => showAddPhoto = true}>
          <Icon name="plus"/>
        </button>
        <button className="btn{showDeletePhoto ? ' btn-dark' : ''}" on:click={() => showDeletePhoto = !showDeletePhoto}>
          <Icon name="trash"/>
        </button>*/}
      </div>
    </nav>
  )
}

export default Navbar